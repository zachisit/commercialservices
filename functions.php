<?php
	//add_filter( 'show_admin_bar', false );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'menus' );
	
	
	/*function add_my_editor_style() {	
		add_editor_style();
	}
	add_action( 'admin_init', 'add_my_editor_style' );
	
	function myformatTinyMCE($in) {
	 $in['plugins']='image,charmap,hr,media,paste,tabfocus,fullscreen,wordpress,wpeditimage,wpgallery,wplink,wpdialogs,wpview';
	 return $in;
	}
	add_filter('tiny_mce_before_init', 'myformatTinyMCE' );*/

	
	register_nav_menus( array(
        'main-menu'  => 'Main menu',
		'heating-cooling-home-menu'  => 'Heating & Cooling Home menu',
		'plumbing-home-menu'  => 'Plumbing Home menu',
		'plumbing-commercial-menu'  => 'Plumbing Commercial Page Menu',
		'plumbing-residential-menu'  => 'Plumbing Residential Page Menu',
		'heating-cooling-commercial-menu'  => 'Heating & Cooling Commercial Page Menu',
		'heating-cooling-residential-menu'  => 'Heating & Cooling Residential Page Menu'
    ) );
	
	
	// Add the role of manager to use for clients
	add_role( 'manager', 'Manager');
	
	function add_theme_caps() {
    // gets the manager role
    $role = get_role( 'manager' );

    // This only works, because it accesses the class instance.
    // would allow the author to edit others' posts for current theme only
	$role->add_cap('delete_others_pages');
	$role->add_cap('delete_others_posts');
	$role->add_cap('delete_pages');
	$role->add_cap('delete_posts');
	$role->add_cap('delete_private_pages');
	$role->add_cap('delete_private_posts');
	$role->add_cap('delete_published_pages');
	$role->add_cap('delete_published_posts');
	$role->add_cap('edit_others_pages');
	$role->add_cap('edit_others_posts');
	$role->add_cap('edit_pages');
	$role->add_cap('edit_posts');
	$role->add_cap('edit_private_pages');
	$role->add_cap('edit_private_posts');
	$role->add_cap('edit_published_pages');
	$role->add_cap('edit_published_posts');
	$role->add_cap('edit_theme_options');
	$role->add_cap('list_users');
	$role->add_cap('manage_categories');
	$role->add_cap('manage_options');
	$role->add_cap('moderate_comments');
	$role->add_cap('publish_pages');
	$role->add_cap('publish_posts');
	$role->add_cap('read_private_pages');
	$role->add_cap('read_private_posts');
	$role->add_cap('read');
	$role->add_cap('remove_users');
	$role->add_cap('upload_files');
	$role->add_cap('edit_users');
	$role->add_cap('create_users');
	$role->add_cap('delete_users');
	$role->add_cap('unfiltered_html');
	$role->add_cap('level_10');
	$role->add_cap('level_9');
	$role->add_cap('level_8');
	$role->add_cap('level_7');
	$role->add_cap('level_6');
	$role->add_cap('level_5');
	$role->add_cap('level_4');
	$role->add_cap('level_3');
	$role->add_cap('level_2');
	$role->add_cap('level_1');
	$role->add_cap('level_0');
	$role->add_cap('NextGEN Gallery overview');
	$role->add_cap('NextGEN Use TinyMCE');
	$role->add_cap('NextGEN Upload images');
	$role->add_cap('NextGEN Manage gallery');
	$role->add_cap('NextGEN Manage tags');
	$role->add_cap('NextGEN Manage others gallery');
	$role->add_cap('NextGEN Edit album');
	$role->add_cap('NextGEN Change style');
	$role->add_cap('NextGEN Change options');
	$role->add_cap('NextGEN Attach Interface');
	}
	add_action( 'admin_init', 'add_theme_caps');
	
	class JPB_User_Caps {

	  // Add our filters
	  function JPB_User_Caps(){
		add_filter( 'editable_roles', array(&$this, 'editable_roles'));
		add_filter( 'map_meta_cap', array(&$this, 'map_meta_cap'),10,4);
	  }

	  // Remove 'Administrator' from the list of roles if the current user is not an admin
	  function editable_roles( $roles ){
		if( isset( $roles['administrator'] ) && !current_user_can('administrator') ){
		  unset( $roles['administrator']);
		}
		return $roles;
	  }

	  // If someone is trying to edit or delete and admin and that user isn't an admin, don't allow it
	  function map_meta_cap( $caps, $cap, $user_id, $args ){
	
		switch( $cap ){
			case 'edit_user':
			case 'remove_user':
			case 'promote_user':
				if( isset($args[0]) && $args[0] == $user_id )
					break;
				elseif( !isset($args[0]) )
					$caps[] = 'do_not_allow';
				$other = new WP_User( absint($args[0]) );
				if( $other->has_cap( 'administrator' ) ){
					if(!current_user_can('administrator')){
						$caps[] = 'do_not_allow';
					}
				}
				break;
			case 'delete_user':
			case 'delete_users':
				if( !isset($args[0]) )
					break;
				$other = new WP_User( absint($args[0]) );
				if( $other->has_cap( 'administrator' ) ){
					if(!current_user_can('administrator')){
						$caps[] = 'do_not_allow';
					}
				}
				break;
			default:
				break;
		}
		return $caps;
	  }

	}
	
	$jpb_user_caps = new JPB_User_Caps();
	
	
	function mw_wp_title( $title, $sep ) {
		global $paged, $page;
	
		if ( is_feed() ) {
			return $title;
		}
	
		// Add the site name.
		$title .= get_bloginfo( 'name' );
	
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
		  $title = '$title $sep $site_description';
		}elseif ( is_404() ) { 
		  $title = '$title $sep Not Found'; 
		}
	
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 ) {
			$title = "$title $sep " . sprintf( 'Page %s', max( $paged, $page ) );
		}
	
		return $title;
	}
	add_filter( 'wp_title', 'mw_wp_title', 10, 2 );

	// Get the page number
	function get_page_number() {
	    if ( get_query_var('paged') ) {
	        print ' | Page ' . get_query_var('paged');
	    }
	} // end get_page_number

	// Custom callback to list comments in this theme's style
	function custom_comments($comment, $args, $depth) {
	  $GLOBALS['comment'] = $comment;
	  $GLOBALS['comment_depth'] = $depth;
	  ?>

      <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
        <div class="comment-author vcard">
          <?php commenter_link() ?>
        </div>
        <div class="comment-meta"><?php printf('Posted %1$s at %2$s <span class="meta-sep">|</span> <a href="%3$s" title="Permalink to this comment">Permalink</a>',
                              get_comment_date(),
                              get_comment_time(),
                              '#comment-' . get_comment_ID() );
                              edit_comment_link('Edit', ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?>
        </div>
        <?php if ($comment->comment_approved == '0') { echo "\t\t\t\t\t<span class='unapproved'>Your comment is awaiting moderation.</span>\n"; } ?>
        <div class="comment-content"><?php comment_text() ?></div>
        <?php // echo the comment reply link
		  if($args['type'] == 'all' || get_comment_type() == 'comment') :
			  comment_reply_link(array_merge($args, array(
				  'reply_text' => 'Reply',
				  'login_text' => 'Log in to reply.',
				  'depth' => $depth,
				  'before' => '<div class="comment-reply-link">',
				  'after' => '</div>'
			  )));
		  endif;
		?>
	<?php } // end custom_comments
	
	// Custom callback to list pings
	function custom_pings($comment, $args, $depth) {
	  $GLOBALS['comment'] = $comment;
	  ?>
      <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
        <div class="comment-author"><?php printf('By %1$s on %2$s at %3$s',
                                  get_comment_author_link(),
                                  get_comment_date(),
                                  get_comment_time() );
                                  edit_comment_link('Edit', ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?>
        </div>
		<?php if ($comment->comment_approved == '0') { echo '\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n'; } ?>
        <div class="comment-content"><?php comment_text() ?></div>
	<?php } // end custom_pings
	
	// Produces an avatar image with the hCard-compliant photo class
	function commenter_link() {
	    $commenter = get_comment_author_link();
	    if ( ereg( '<a[^>]* class=[^>]+>', $commenter ) ) {
	        $commenter = ereg_replace( '(<a[^>]* class=[\'"]?)', '\\1url ' , $commenter );
	    } else {
	        $commenter = ereg_replace( '(<a )/', '\\1class="url "' , $commenter );
	    }
	    $avatar_email = get_comment_author_email();
	    $avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( $avatar_email, 80 ) );
	    echo $avatar . ' <span class="fn n">' . $commenter . '</span>';
	} // end commenter_link
	
	// For category lists on category archives: Returns other categories except the current one (redundant)
	function cats_meow($glue) {
	    $current_cat = single_cat_title( '', false );
	    $separator = "\n";
	    $cats = explode( $separator, get_the_category_list($separator) );
	    foreach ( $cats as $i => $str ) {
	        if ( strstr( $str, ">$current_cat<" ) ) {
	            unset($cats[$i]);
	            break;
	        }
	    }
	    if ( empty($cats) )
	        return false;

	    return trim(join( $glue, $cats ));
	} // end cats_meow
	
	// For tag lists on tag archives: Returns other tags except the current one (redundant)
	function tag_ur_it($glue) {
	    $current_tag = single_tag_title( '', '',  false );
	    $separator = "\n";
	    $tags = explode( $separator, get_the_tag_list( "", "$separator", "" ) );
	    foreach ( $tags as $i => $str ) {
	        if ( strstr( $str, ">$current_tag<" ) ) {
	            unset($tags[$i]);
	            break;
	        }
	    }
	    if ( empty($tags) )
	        return false;

	    return trim(join( $glue, $tags ));
	} // end tag_ur_it
	
	// Register widgetized areas
	function theme_widgets_init() {
	    // Area 1
	    register_sidebar( array (
	    'name' => 'Sidebar Widgets',
	    'id' => 'sidebar_widgets',
	    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	    'after_widget' => '</li>',
	    'before_title' => '<h3 class="widget-title">',
	    'after_title' => '</h3>',
	  ) );
	    // Area 2
	    register_sidebar( array (
	    'name' => 'Specials Sidebar Widgets',
	    'id' => 'spec_sidebar_widgets',
	    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	    'after_widget' => '</li>',
	    'before_title' => '<h3 class="widget-title">',
	    'after_title' => '</h3>',
	  ) );

	} // end theme_widgets_init

	add_action( 'init', 'theme_widgets_init' );
	
	$preset_widgets = array (
	    'primary_widget_area'  => array( 'search' ),
	    'secondary_widget_area'  => array()
	);
	if ( isset( $_GET['activated'] ) ) {
	    update_option( 'sidebars_widgets', $preset_widgets );
	}
	
	// Check for static widgets in widget-ready areas
	function is_sidebar_active( $index ){
	  global $wp_registered_sidebars;
	  $widgetcolums = wp_get_sidebars_widgets();
	  if ($widgetcolums[$index]) return true;
	  return false;
	} // end is_sidebar_active
	
	function excerpt($limit) {
	  $excerpt = explode(' ', get_the_excerpt(), $limit);
	  if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'<br /><a href="'. get_permalink($post->ID) . '" class="rmore">MORE</a>';
	  } else {
		$excerpt = implode(" ",$excerpt);
	  }	
	  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	  return $excerpt;
	}
	
	function new_excerpt_more($more) {
		   global $post;
		return ' ...<a href="'. get_permalink($post->ID) . '">continue-></a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
	
	function excerpt_read_more_link($output) {
	 global $post;
	 return $output . '<a href="'. get_permalink($post->ID) . '" class="rmore">read more</a>';
	}
	add_filter('the_excerpt', 'excerpt_read_more_link');
	
	
	/* START STYLE AND SCRIPT SUPPORT */
	
	function theme_support_start_styles() {
		wp_enqueue_style( 'boilerplate', get_template_directory_uri() . '/css/boilerplate.css', array(), '1.0.1' );
	}
	add_action( 'wp_enqueue_scripts', 'theme_support_start_styles', 0 );
	
	function theme_support_comment_scripts() {
		if ( is_singular() && comments_open() ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'theme_support_comment_scripts' );
	
	function theme_support_end_styles() {
		wp_enqueue_style( 'maincss', get_template_directory_uri() . '/css/main.css', array('boilerplate') );
		/*wp_enqueue_style( 'maincss', get_template_directory_uri() . '/css/main.css', array('boilerplate'), '1.0.1' );*/
	}
	add_action( 'wp_enqueue_scripts', 'theme_support_end_styles', 3000 );
	
	function theme_support_end_scripts() {
		wp_enqueue_script( 'respond_js', get_template_directory_uri() . '/js/respond.min.js', array(), '1.0.1' );
	}
	add_action( 'wp_enqueue_scripts', 'theme_support_end_scripts', 3000 );
	
	function theme_support_footer_scripts() {
		wp_enqueue_script( 'theme_support_js', get_template_directory_uri() . '/js/theme_support.js', array( 'jquery' ), '1.0.1', true );
	}
	add_action( 'wp_enqueue_scripts', 'theme_support_footer_scripts', 333 );
	
	/* END STYLE AND SCRIPT SUPPORT */
	
	function admin_login_logo() {
?>
	  <style type="text/css">
          body.login div#login h1 a {
              background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/admin_login_logo.png);
          }
      </style>
<?php
	}
	add_action( 'login_enqueue_scripts', 'admin_login_logo' );
	
	function my_login_stylesheet() { ?>
        <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_bloginfo( 'stylesheet_directory' ) . '/css/login.css'; ?>" type="text/css" media="all" />
    <?php }
    add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
	
	function admin_login_logo_url() {
	  return get_bloginfo( 'url' );
	}
	add_filter( 'login_headerurl', 'admin_login_logo_url' );
	
	function admin_login_logo_url_title() {
	  return get_bloginfo( 'name' );
	}
	add_filter( 'login_headertitle', 'admin_login_logo_url_title' );
	
	// add logo to admin pages
	function add_admin_page_logo() {
		wp_enqueue_script( 'admin_page_logo', get_bloginfo( 'template_directory' ) . '/js/admin_logo.js', array( 'jquery' ), '1.0.0', true );
		$tpath = array( 'tpathurl' => get_bloginfo( 'template_directory' ) );
		wp_localize_script( 'admin_page_logo', 'object_name', $tpath );
	}
	add_action( 'admin_enqueue_scripts', 'add_admin_page_logo' );
	
	function additional_admin_color_schemes() {  
		//Get the theme directory  
		$theme_dir = get_template_directory_uri();  
	  
		//Ocean  
		wp_admin_css_color( 'commsrv', 'Commercail Service',  
			$theme_dir . '/css/admin_colors.css',  
			array( '#00287b', '#486bbb', '#ccd4dd', '#990000' ) 
		);  
	}  
	add_action('admin_init', 'additional_admin_color_schemes'); 
	
	function set_default_admin_color($user_id) {
		$args = array(
			'ID' => $user_id,
			'admin_color' => 'commsrv'
		);
		wp_update_user( $args );
	}
	add_action('user_register', 'set_default_admin_color');
	
	function angieslist_code( $atts ){
		return '<div class="angieimg"><script type="text/javascript" src="http://www.angieslist.com/webbadge/insertwebbadge.js?bid=c5feae325095b87c410727e692adb3d8"></script><script type="text/javascript">if (BADGEBOX) BADGEBOX.PlaceBadge();</script><noscript><div id="ssanslnk"><a href="http://www.angieslist.com/companylist/us/in/bloomington/commercial-service-reviews-2312781.aspx" title="Angie\'s List Super Service Award winner">BLOOMINGTON heating & air conditioning/hvac</a></div></noscript></div>';
	}
	add_shortcode( 'angieslist', 'angieslist_code' );
	
	function copyright_code( $atts ){
		$cdate = date("Y");
		return '&copy; 2013 - ' . $cdate;
	}
	add_shortcode( 'copyright', 'copyright_code' );
	
	
	
	function mce_mod( $init ) {
	
		$style_formats = array (
			array( 'title' => 'Clear All', 'classes' => 'clear_all', 'block' => 'div' ),
			array( 'title' => 'Two Column Left', 'classes' => 'two_col_left', 'selector' => 'div', 'block' => 'div' ),
			array( 'title' => 'Two Column Right', 'classes' => 'two_col_right', 'selector' => 'div', 'block' => 'div' ),
			array( 'title' => 'Three Column Left', 'classes' => 'three_col_left', 'selector' => 'div', 'block' => 'div' ),
			array( 'title' => 'Three Column Middle', 'classes' => 'three_col_middle', 'selector' => 'div', 'block' => 'div' ),
			array( 'title' => 'Three Column Right', 'classes' => 'three_col_right', 'selector' => 'div', 'block' => 'div' ),
			array( 'title' => 'Print Image', 'classes' => 'printimg', 'selector' => 'img' ),
		);
	
		$init['style_formats'] = json_encode( $style_formats );
	
		$init['style_formats_merge'] = true;
		return $init;
	}
	add_filter('tiny_mce_before_init', 'mce_mod');
	
	function theme_add_editor_styles() {
		add_editor_style( 'editor-style.css' );
	}
	add_action( 'init', 'theme_add_editor_styles' );
	
?>
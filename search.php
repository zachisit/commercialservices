<?php get_header(); ?>
 
        <div id="container">
            <div id="content">
 
				<?php if ( have_posts() ) : ?>

				                <h1 class="page-title">Search Results for: <span><?php the_search_query(); ?></span></h1>                     

				<?php while ( have_posts() ) : the_post() ?>

				                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf('Permalink to %s', the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

				<?php if ( $post->post_type == 'post' ) { ?>
				                    <div class="entry-meta">
				                        <span class="meta-prep meta-prep-entry-date">Published </span>
				                        <span class="entry-date"><abbr class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>
				                    </div><!-- .entry-meta -->
				<?php } ?>

				                    <div class="entry-summary">
									  <?php the_excerpt(); ?>
                                      <?php wp_link_pages('before=<div class="page-link">Pages:&after=</div>') ?>
				                    </div><!-- .entry-summary -->
                                    <div class="clear_all"></div>
				                </div><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; ?>

				<?php global $wp_query; $total_pages = $wp_query->max_num_pages; if ( $total_pages > 1 ) { ?>
				                <div id="nav-below" class="navigation">
				                    <div class="nav-previous"><?php next_posts_link('<span class="meta-nav">&laquo;</span> previous' ) ?></div>
				                    <div class="nav-next"><?php previous_posts_link('next <span class="meta-nav">&raquo;</span>' ) ?></div>
                                    <div class="clear_all"></div>
				                </div><!-- #nav-below -->
				<?php } ?>            

				<?php else : ?>

				                <div id="post-0" class="post no-results not-found">
				                    <h2 class="entry-title">Nothing Found</h2>
				                    <div class="entry-content">
				                        <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
										<?php get_search_form(); ?>
				                    </div><!-- .entry-content -->
				                </div>

				<?php endif; ?>           
 
            </div><!-- #content -->
			<?php get_sidebar(); ?>
            <div class="clear_all"></div>
        </div><!-- #container -->
 
<?php get_footer(); ?>
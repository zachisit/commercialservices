<?php get_header(); ?>
 
        <div id="container">
            <div id="content">
 
                <?php the_post(); ?>          

				<h1 class="page-title"><?php single_tag_title() ?></h1>

				<?php rewind_posts(); ?>         

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( 'Permalink to %s', the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

				                    <div class="entry-meta">
				                        <span class="meta-prep meta-prep-author">By </span>
				                        <span class="author vcard"><a class="url fn n" href="<?php echo get_author_link( false, $authordata->ID, $authordata->user_nicename ); ?>" title="<?php printf( 'View all posts by %s', $authordata->display_name ); ?>"><?php the_author(); ?></a></span>
				                        <span class="meta-sep"> | </span>
				                        <span class="meta-prep meta-prep-entry-date">Published </span>
				                        <span class="entry-date"><abbr class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>
				                        <?php edit_post_link( 'Edit', "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t" ) ?>
				                    </div><!-- .entry-meta -->

				                    <div class="entry-summary">
									  <?php the_excerpt(); ?>
				                    </div><!-- .entry-summary -->

				                    <div class="entry-utility">
				                        <span class="cat-links"><span class="entry-utility-prep entry-utility-prep-cat-links">Posted in </span><?php echo get_the_category_list(', '); ?></span>
				                        <span class="meta-sep"> | </span>
				                        <?php if ( $tag_ur_it = tag_ur_it(', ') ) : // Returns tags other than the one queried ?>
										                        <span class="tag-links"><?php printf( 'Also tagged %s', $tag_ur_it ) ?></span>
										<?php endif; ?>
				                        <span class="comments-link"><?php comments_popup_link( 'Leave a comment', '1 Comment', '% Comments' ) ?></span>
				                        <?php edit_post_link( 'Edit', "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t\n" ) ?>
				                    </div><!-- #entry-utility -->
				                </div><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; else: ?>
                <p>Sorry, there are no articles in this category.</p>
                <?php endif; ?>            

				<?php global $wp_query; $total_pages = $wp_query->max_num_pages; if ( $total_pages > 1 ) { ?>
				                <div id="nav-below" class="navigation">
				                    <div class="nav-previous"><?php next_posts_link('<span class="meta-nav">&laquo;</span> previous') ?></div>
				                    <div class="nav-next"><?php previous_posts_link('next <span class="meta-nav">&raquo;</span>') ?></div>
				                </div><!-- #nav-below -->
				<?php } ?>                 
 
            </div><!-- #content -->
			<?php get_sidebar(); ?>
            <div class="clear_all"></div>
        </div><!-- #container -->

<?php get_footer(); ?>
<?php get_header(); ?>
 
        <div id="container">
            <div id="content">
 
                <?php the_post(); ?>          

				<h1 class="page-title author"><?php printf( 'Author: <span class="vcard">%s</span>', "<a class='url fn n' href='$authordata->user_url' title='$authordata->display_name' rel='me'>$authordata->display_name</a>" ) ?></h1>
				<?php $authordesc = $authordata->user_description; if ( !empty($authordesc) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $authordesc . '</div>' ); ?>

				<?php rewind_posts(); ?>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf('Permalink to %s', the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

				                    <div class="entry-meta">
				                        <span class="meta-prep meta-prep-entry-date">Published </span>
				                        <span class="entry-date"><abbr class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>
				                    </div><!-- .entry-meta -->

				                    <div class="entry-summary">
									  <?php the_excerpt(); ?>
				                    </div><!-- .entry-summary -->
                                    <div class="clear_all"></div>
				                </div><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; else: ?>
                <p>Sorry, there are no articles in this category.</p>
                <?php endif; ?>                

				<?php global $wp_query; $total_pages = $wp_query->max_num_pages; if ( $total_pages > 1 ) { ?>
				                <div id="nav-below" class="navigation">
				                    <div class="nav-previous"><?php next_posts_link('<span class="meta-nav">&laquo;</span> Older' ) ?></div>
				                    <div class="nav-next"><?php previous_posts_link('Newer <span class="meta-nav">&raquo;</span>' ) ?></div>
                                    <div class="clear_all"></div>
				                </div><!-- #nav-below -->
				<?php } ?>               
 
            </div><!-- #content -->
			<?php get_sidebar(); ?>
            <div class="clear_all"></div>
        </div><!-- #container -->
 
<?php get_footer(); ?>
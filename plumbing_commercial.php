<?php
/*
Template Name: Plumbing Commercial
*/
?>
<?php get_header(); ?>
 
        <div id="container">
            <div id="section_sidebar">
              <div id="division_header">
                <?php echo'<img src="'. ot_get_option('plumbing_image') . '" />'; ?>
                <div id="division_title">PLUMBING</div>
              </div>
              <div id="section_header">
                <?php echo'<img src="'. ot_get_option('commercial_img') . '" />'; ?>
                <div id="section_title">COMMERCIAL</div>
              </div>
              <div class="clear_all"></div>
            </div>
            <div id="content" class="commercial">
 
<?php the_post(); ?>
 
                <nav id="sectionmenu"><?php wp_nav_menu( array( 'theme_location' => 'plumbing-commercial-menu', 'container_id' => 'sub_menu', 'depth' => '1' ) ); ?></nav>
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <div class="entry-content">
<?php the_content(); ?>
<?php wp_link_pages('before=<div class="page-link">Pages:&after=</div>') ?>
<?php edit_post_link( 'Edit', '<span class="edit-link">', '</span>' ) ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-<?php the_ID(); ?> -->           
 
<?php if ( get_post_custom_values('comments') ) comments_template() // Add a custom field with Name and Value of "comments" to enable comments on this page ?>            
 
            </div><!-- #content -->
			<?php get_sidebar(); ?>
            <div class="clear_all"></div>
        </div><!-- #container -->
 
<?php get_footer(); ?>
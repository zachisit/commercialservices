<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    
	<link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <?php wp_head(); ?>
    
	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<?php
/*
if this is the hague page then let's remove the content background and change the H stylings
this is to be deprecated in future
*/
if (is_page('1589')) { ?>
    <style>
        #content {background-color:white !important;}
        #content h2, #content h3 {color:#08529a;}
        h1.entry-title {display:none !important;}
    </style>
<?php } ?>
</head>
<body <?php body_class() ?> id="commsrv">
<div id="white_wrap" <?php if ((is_page_template('heating_cooling_residential.php')) || (is_page_template('plumbing_residential.php'))) { echo 'class="residential"'; } ?>>
  <header>
    <div id="commsrv_logo"><a href="<?php echo bloginfo('url'); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/logos/april_logo.png" alt="Commercial Service" id="cs_logo"></a></div>
    <div id="menu_btn"><a href="#" id="mbtn"><img src="<?php bloginfo( 'template_url' ); ?>/images/menu_icon.png" alt="menu"></a></div>
    <div id="nav_contact">
      <a href="#" id="menu_close">X</a>
      <nav id="main_nav" <?php if ((is_page_template('heating_cooling_residential.php')) || (is_page_template('plumbing_residential.php'))) { echo 'class="green_bg"'; } else { echo 'class="red_bg"'; } ?> >
		<div id="badges_full">
		  <?php if ( function_exists( 'ot_get_option' ) ) {
			echo do_shortcode(ot_get_option( 'badges' ));
		  } ?>
        </div>
		<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'container_id' => 'main_menu', 'depth' => '2' ) ); ?>
        <div class="clear_all"></div>
      </nav>
      <div id="contact_box">
        <div id="contact_email">
          <div class="top_pad"></div>
          <div id="email_label">EMAIL</div>
          <div id="email_addy"><a href="mailto:info@commsrv.com" target="_blank">info@commsrv.com</a></div>
        </div>
        <div id="contact_phone_address">
          <div class="top_pad"></div>
          <div id="phonenumber"><a href="tel:812-339-9114" class="ph_number">812.318.1477</a></div>
          <div id="address">1833 S Curry Pike<span id="address_break">,&nbsp;&nbsp;</span>Bloomington IN 47403</div>
        </div>
        <div id="badges_small">
		  <?php if ( function_exists( 'ot_get_option' ) ) {
			echo ot_get_option( 'badges' );
		  } ?>
        </div>
        <div class="clear_all"></div>
      </div>
      <div class="clear_all"></div>
    </div>
    <div class="clear_all"></div>
  </header>
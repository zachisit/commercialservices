<div id="sidebar" role="complementary">

<?php if ( is_sidebar_active('spec_sidebar_widgets') ) : ?>
        <div id="sb_widgets" class="widget-area">
            <ul class="xoxo">
                <?php dynamic_sidebar('spec_sidebar_widgets'); ?>
            </ul>
        </div><!-- #sb_widgets .widget-area -->
<?php endif; ?>

</div><!-- #sidebar -->
  <footer>
    <div id="contact_footer">
      <?php if ( function_exists( 'ot_get_option' ) ) {
		echo ot_get_option( 'spec_contact_footer' );
	  } ?></div>
    <div id="footer_logos">
      <?php if ( function_exists( 'ot_get_option' ) ) {
		echo ot_get_option( 'footer_logos' );
	  } ?>
      <div class="clear_all"></div>
    </div>
    <?php if (is_home()) { 
	  if ( function_exists( 'ot_get_option' ) ) {
		echo '<div id="home_logos">' .  ot_get_option( 'home_logos' ) . do_shortcode('[angieslist]') . '</div>';
	  }
    } ?>
    <div id="footer_decription">
      <?php if ( function_exists( 'ot_get_option' ) ) {
		echo ot_get_option( 'gray_text' );
	  } ?>
    </div>
  </footer>
</div>
<div id="mw_footer">
  <div class="greenweb">
    <a href="http://www.accountsupport.com/green-certified/commsrv.com" onClick="MyWindow=window.open('http://www.accountsupport.com/green-certified/commsrv.com','greenCertified','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=550,height=700,left=50,top=50'); return false;">
      <img src="http://www.accountsupport.com/green-certified/hosting-badge-12.png" border="0">
    </a>
  </div>
  <div id="mwtext">
    <a href="http://www.mediaworksonline.com" target="_blank">Website Design by Mediaworks Advertising Agency - Bloomington, IN</a>
  </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
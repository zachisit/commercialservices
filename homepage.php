<?php
/**
 * Template Name: Page - Home Page
 */
get_header(); ?>

<div class="row custom">
<div class="six columns commercial">


<?php
$args = array(
	'pagename' => 'heating-cooling'
); ?>
<?php $my_query = new WP_Query($args); ?>
<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>


<div class="caption">
<h2 class="white">Heating & Cooling</h2>
<?php wp_nav_menu( array( 'theme_location' => 'heating-cooling-home-menu', 'container_id' => 'commercial_menu', 'depth' => '0' ) ); ?>
</div>

<?php endwhile; ?>

</div>



<div class="six columns residential">


<?php
$args = array(
	'pagename' => 'plumbing'
); ?>
<?php $my_query = new WP_Query($args); ?>
<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>


<div class="caption">
<h2 class="white">Plumbing</h2>
<?php wp_nav_menu( array( 'theme_location' => 'plumbing-home-menu', 'container_id' => 'residential_menu', 'depth' => '0' ) ); ?>
</div>

<?php endwhile; ?>

</div>

</div>
 
<div class="row p_10">
<?php while (have_posts()) : the_post(); ?><?php the_content(); ?><?php endwhile; // end of the loop. ?>
</div>

<?php get_footer(); ?>
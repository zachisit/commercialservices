<?php get_header(); ?>

<div id="container">
    <div id="portal_wrap">
        <div id="portal_wrap_left">
            <img src="<?php bloginfo('template_url'); ?>/images/70years.png" alt="Celebrating 70 Years" />
            <p>We are now southern Indiana's most trusted Hague Quality Water dealer.</p>
            <div class="two">
                <a href="http://www.commercialservice.com/hague-quality-water-international/" title="Hague Water International"><img src="<?php bloginfo('template_url'); ?>/images/hague.png" alt="Hague Quality Water International" /></a>
                <div class="bottom">
                    <div class="left"><p>Call us now for a <strong>FREE</strong><br />in-home water quality test</p></div>
                    <div class="right"><a href="http://www.commercialservice.com/hague-quality-water-international/" title="Learn More">Learn More</a></div>
                </div>
            </div>
        </div>
      <div id="portal_wrap_right">
         <div id="hvac_portal">
        <?php //echo'<img src="'. ot_get_option('hc_image') . '" />'; ?>
        <img src="<?php bloginfo( 'template_url' ); ?>/images/april_one.png" alt="Heading and Cooling" />
        <div class="portal_title">HEATING &amp; COOLING</div>
        <?php wp_nav_menu( array( 'theme_location' => 'heating-cooling-home-menu', 'container_id' => 'hvac_menu', 'container_class' => 'portal_menu', 'depth' => '1' ) ); ?>
      </div>
      <div id="plumbing_portal">
        <?php //echo'<img src="'. ot_get_option('plumbing_image') . '" />'; ?>
          <img src="<?php bloginfo( 'template_url' ); ?>/images/april_two.png" alt="Plumbing" />
        <div class="portal_title">PLUMBING</div>
        <?php wp_nav_menu( array( 'theme_location' => 'plumbing-home-menu', 'container_id' => 'plumbing_menu', 'container_class' => 'portal_menu', 'depth' => '1' ) ); ?>
      </div>       
        </div>

      <div class="clear_all"></div>
    </div>
    <div id="specials_wrap">
      <div id="specials_left">
		<div id="special1" class="special_wrap">
		  <?php $special1_link = get_permalink( ot_get_option('special1_link') ); ?>
          <?php echo'<a href="'. $special1_link . '"><img src="'. ot_get_option('special1_img') . '" /></a>'; ?>
        </div>
		<div id="special2" class="special_wrap">
		  <?php $special2_link = get_permalink( ot_get_option('special2_link') ); ?>
          <?php echo'<a href="'. $special2_link . '"><img src="'. ot_get_option('special2_img') . '" /></a>'; ?>
        </div>
        <div class="clear_all"></div>
      </div>
      <div id="specials_right">
		<div id="special3" class="special_wrap">
		  <?php $special3_link = get_permalink( ot_get_option('special3_link') ); ?>
          <?php echo'<a href="'. $special3_link . '"><img src="'. ot_get_option('special3_img') . '" /></a>'; ?>
        </div>
		<div id="special4" class="special_wrap">
		  <?php $special4_link = get_permalink( ot_get_option('special4_link') ); ?>
          <?php echo'<a href="'. $special4_link . '"><img src="'. ot_get_option('special4_img') . '" /></a>'; ?>
        </div>
        <div class="clear_all"></div>
      </div>
      <div class="clear_all"></div>
    </div>
    <h1 id="home_title">
    <?php if ( function_exists( 'ot_get_option' ) ) {
	  echo ot_get_option( 'home_page_title' );
	} ?>
    </h1>
    <div id="home_content">
    <?php if ( function_exists( 'ot_get_option' ) ) {
	  echo ot_get_option( 'home_content' );
	} ?>
    </div>
    <div id="home_bullet_list">
    <?php if ( function_exists( 'ot_get_option' ) ) {
	  echo ot_get_option( 'home_bullet_list' );
	} ?>
    <div class="clear_all"></div>
    </div>
    <div id="home_spec_btns">
      <div id="online_payment_btn" class="home_spec_btn">
        <a target="_blank" href="<?php echo bloginfo('url'); ?>/online-payment/">
          Online Payment
        </a>
      </div>
      <div id="res_finance" class="home_spec_btn">
        <a target="_blank" href="https://retailservices.wellsfargo.com/retailprivatelabel/initApp.do?profileNum=2710&amp;dealerId=717416552">
          Click Here for Residential Financing
        </a>
      </div>
      <div class="clear_all"></div>
    </div>
    <div class="clear_all"></div>
</div><!-- #container -->
 
<?php get_footer(); ?>
<?php
/*
Template Name: Heating & Cooling Portal
*/
?>
<?php get_header(); ?>
 
        <div id="container">
            <div id="section_sidebar">
              <div id="division_header">
                <?php echo'<img src="'. ot_get_option('hc_image') . '" />'; ?>
                <div id="division_title">HEATING &amp; COOLING</div>
              </div>
              <div class="clear_all"></div>
            </div>
            <div id="content">
 
<?php the_post(); ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <div class="entry-content">
					  <?php the_content(); ?>
                      <div id="portal_options">
                        <div id="section_header">
						  <a href="<?php echo bloginfo('url'); ?>/heating-cooling/residential-heating-cooling/">
						  <?php echo'<img src="'. ot_get_option('residential_img') . '" />'; ?></a>
                          <div id="section_title">RESIDENTIAL</div>
                        </div>
                        <?php wp_nav_menu( array( 'theme_location' => 'heating-cooling-residential-menu', 'container' => 'nav', 'container_id' => 'portal_menu', 'depth' => '1' ) ); ?>
                        <div id="section_divider"></div>
                        <div id="section_header">
						  <a href="<?php echo bloginfo('url'); ?>/heating-cooling/commercial-heating-cooling/">
						  <?php echo'<img src="'. ot_get_option('commercial_img') . '" />'; ?></a>
                          <div id="section_title">COMMERCIAL</div>
                        </div>
                        <?php wp_nav_menu( array( 'theme_location' => 'heating-cooling-commercial-menu', 'container' => 'nav', 'container_id' => 'portal_menu', 'depth' => '1' ) ); ?>
                      </div>
                      <?php wp_link_pages('before=<div class="page-link">Pages:&after=</div>') ?>
                      <?php edit_post_link( 'Edit', '<span class="edit-link">', '</span>' ) ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-<?php the_ID(); ?> -->           
 
<?php if ( get_post_custom_values('comments') ) comments_template() // Add a custom field with Name and Value of "comments" to enable comments on this page ?>            
 
            </div><!-- #content -->
			<?php get_sidebar(); ?>
            <div class="clear_all"></div>
        </div><!-- #container -->
 
<?php get_footer(); ?>
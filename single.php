<?php get_header(); ?>
 
        <div id="container">
            <div id="content">

				<?php the_post(); ?>

                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<h1 class="entry-title"><?php the_title(); ?></h1>
					
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages('before=<div class="page-link">Pages:&after=</div>') ?>
					</div><!-- .entry-utility -->
					
					<div class="entry-utility">

					<?php if ('open' == $post->comment_status) : // Comments open ?>
					                        <?php printf('<a class="comment-link" href="#respond" title="Post a comment">Post a comment</a>', get_trackback_url() ) ?>
					<?php endif; ?>
					<?php edit_post_link('Edit', "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ) ?>
                    </div>
                </div><!-- #post-<?php the_ID(); ?> -->           
 
                <div id="nav-below" class="navigation">
                  <div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">&laquo;</span> Older', TRUE ) ?></div>
                  <div class="nav-next"><?php next_post_link( '%link', 'Newer <span class="meta-nav">&raquo;</span>', TRUE ) ?></div>
                  <div class="clear_all"></div>
				</div><!-- #nav-below -->     
            
 				<?php comments_template('', true); ?>

            </div><!-- #content -->
			<?php get_sidebar(); ?>
            <div class="clear_all"></div>
        </div><!-- #container -->
 
<?php get_footer(); ?>
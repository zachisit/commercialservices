  <footer>
    <!--<div id="service_call_bar" <?php if ((is_page_template('heating_cooling_residential.php')) || (is_page_template('plumbing_residential.php'))) { echo 'class="green_service"'; } else { echo 'class="red_service"'; } ?>>
    
	<?php if ((is_page_template('heating_cooling_residential.php')) || (is_page_template('plumbing_residential.php'))) { ?>
      <div id="service_image"><img src="<?php bloginfo( 'template_url' ); ?>/images/greenteam_callout.png"></div>
      <div id="service_left">Residential <strong>"Green"</strong> Team</div>
      <div id="service_right">Call us today <storng>812.318.1477</strong></div>
      <div class="clear_all"></div>
	<?php } else {  ?>
      <div id="service_image"><img src="<?php bloginfo( 'template_url' ); ?>/images/commercial_callout.png"></div>
      <div id="service_left">servicing area businesses <span class="nowrap">for over <strong>65</strong> years</span></div>
      <div id="service_right"><strong>24 Hour</strong> service available<br />Call us today <strong>812.318.1477</strong></div>
      <div class="clear_all"></div>
	<?php } ?>
    
    </div>
    -->
    <div id="service_call_bar">
        <div class="left">
            <div class="twentyfourseven"><img src="<?php bloginfo( 'template_url' ); ?>/images/red_footer_247.png" alt="available 24/7" /></div>
            <div class="top"><strong>24 Hour</strong> service available</div>
            <div class="bottom">Call us today <strong>812.318.1477</strong></div>
        </div>  
        <div class="right">
            <div class="one">Serving</div>
            <div class="two">Bloomington</div>
            <div class="three">Since 1946</div>
        </div>
    </div>
    <div id="footer_decription">
      <?php if ( function_exists( 'ot_get_option' ) ) {
		echo ot_get_option( 'gray_text' );
	  } ?>
    </div>
    <div id="contact_footer">
      <?php if ( function_exists( 'ot_get_option' ) ) {
		echo do_shortcode(ot_get_option( 'contact_footer' ));
	  } ?></div>
    <div id="footer_logos">
      <?php if ( function_exists( 'ot_get_option' ) ) {
		echo ot_get_option( 'footer_logos' );
	  } ?>
      <div class="clear_all"></div>
    </div>
    <?php if (is_home()) { 
	  if ( function_exists( 'ot_get_option' ) ) {
		echo '<div id="home_logos">' .  ot_get_option( 'home_logos' )/* . do_shortcode('[angieslist]')*/ . '</div>';
	  }
    } ?>
  </footer>
</div>
<div id="mw_footer">
  <div class="greenweb">
    <a href="http://www.accountsupport.com/green-certified/commsrv.com" onClick="MyWindow=window.open('http://www.accountsupport.com/green-certified/commsrv.com','greenCertified','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=550,height=700,left=50,top=50'); return false;">
      <img src="http://www.accountsupport.com/green-certified/hosting-badge-12.png" border="0">
    </a>
  </div>
  <div id="mwtext">
    <a href="http://www.mediaworksonline.com" target="_blank">Website Design by Mediaworks Advertising Agency - Bloomington, IN</a>
  </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
jQuery(document).ready(function($){
  
  //Remove dimensions form images
  $('img').each(function(){ 
	$(this).removeAttr('width')
	$(this).removeAttr('height');
  });  
});
  
jQuery(window).load(function($){
  var loadwinwidth = jQuery(window).width();
  if ((loadwinwidth > 640) && (loadwinwidth < 769)) {
	jQuery('#main_nav').height('auto');
	jQuery('#contact_box').height('auto');
	jQuery('#red_bg').height(jQuery('#commsrv_logo').height());
	jQuery('#main_nav').height(jQuery('#commsrv_logo').height());
  } else if (loadwinwidth > 768) {
	jQuery('#content').height() > jQuery('#sidebar').height() ? jQuery('#sidebar').height(jQuery('#content').height()) : jQuery('#content').height(jQuery('#sidebar').height());
	jQuery('#commsrv_logo').height('auto');
	jQuery('#main_nav').height('auto');
	jQuery('#contact_box').height('auto');
	
	if (jQuery('#commsrv_logo').height() > jQuery('#contact_box').height()) {
	  jQuery('#main_nav').height(jQuery('#commsrv_logo').height());
	  jQuery('#contact_box').height(jQuery('#commsrv_logo').height());
	} else if (jQuery('#contact_box').height() > jQuery('#commsrv_logo').height()) {
	  jQuery('#commsrv_logo').height(jQuery('#contact_box').height());
	  jQuery('#main_nav').height(jQuery('#contact_box').height());
	} else if (jQuery('#contact_box').height() == jQuery('#commsrv_logo').height()) {
	  jQuery('#main_nav').height(jQuery('#commsrv_logo').height());
	  jQuery('#contact_box').height(jQuery('#commsrv_logo').height());
	}
  }
  
  
  
});

jQuery(window).resize(function(){
  var winwidth = jQuery(window).width();
  if (winwidth <= 640) {
	  
	jQuery('#content').height('auto');
	jQuery('#sidebar').height('auto');
	jQuery( '#nav_contact' ).css({ display: 'none' });
	jQuery('#commsrv_logo').height('auto');
	jQuery('#main_nav').height('auto');
	jQuery('#contact_box').height('auto');
	
  } else if ((winwidth > 640) && (winwidth < 769)) {
	  
	jQuery('#content').height('auto');
	jQuery('#sidebar').height('auto');
	jQuery( '#nav_contact' ).css({ display: 'block' });
	jQuery('#commsrv_logo').height(jQuery('#cs_logo').height());
	jQuery('#main_nav').height('auto');
	jQuery('#contact_box').height('auto');
	jQuery('#main_nav').height(jQuery('#commsrv_logo').height());
	
  } else if (winwidth > 768) {
	  
	jQuery('#content').height() > jQuery('#sidebar').height() ? jQuery('#sidebar').height(jQuery('#content').height()) : jQuery('#content').height(jQuery('#sidebar').height());
	jQuery( '#nav_contact' ).css({ display: 'block' });
	jQuery('#commsrv_logo').height('auto');
	jQuery('#main_nav').height('auto');
	jQuery('#contact_box').height('auto');
	
	if (jQuery('#commsrv_logo').height() > jQuery('#contact_box').height()) {
		
	   jQuery('#main_nav').height(jQuery('#commsrv_logo').height());
	   jQuery('#contact_box').height(jQuery('#commsrv_logo').height());
	   
	} else if (jQuery('#contact_box').height() > jQuery('#commsrv_logo').height()) {
		
	   jQuery('#commsrv_logo').height(jQuery('#contact_box').height());
	   jQuery('#main_nav').height(jQuery('#contact_box').height());
	   
	} else if (jQuery('#contact_box').height() == jQuery('#commsrv_logo').height()) {
	  jQuery('#main_nav').height(jQuery('#commsrv_logo').height());
	  jQuery('#contact_box').height(jQuery('#commsrv_logo').height());
	}
  }
});

jQuery('#mbtn').click(function() {
  if(jQuery('#nav_contact').is(':visible')) {
	jQuery('#nav_contact').animate({ left: '-100%' }, 'slow', function () { jQuery("#nav_contact").css('display', 'none'); });    
  } else {
	jQuery("#nav_contact").css('display', 'block');
	jQuery('#nav_contact').animate({ left: '0' }, 'slow');
  }
});

jQuery('#menu_close').click(function() {
  jQuery('#nav_contact').animate({ left: '-100%' }, 'slow', function () { jQuery("#nav_contact").css('display', 'none'); });
});
<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage commercialservice
 */

get_header(); ?>
 
        <div id="container">
            <div id="content">

                <article id="post-0" class="post error404 no-results not-found">
                    <header class="entry-header">
                        <h1 class="entry-title">Not Found</h1>
                    </header>
    
                    <div class="entry-content">
                        <p>It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.</p>
                        <?php get_search_form(); ?>
                    </div><!-- .entry-content -->
                </article><!-- #post-0 -->

            </div><!-- #content -->
			<?php get_sidebar(); ?>
            <div class="clear_all"></div>
        </div><!-- #container -->
 
<?php get_footer(); ?>